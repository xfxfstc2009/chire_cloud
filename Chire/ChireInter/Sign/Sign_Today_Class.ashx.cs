﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Newtonsoft.Json;

namespace Chire.ChireInter.Sign
{
    /// <summary>
    /// Sign_Today_Class 的摘要说明
    /// </summary>
    public class Sign_Today_Class : IHttpHandler
    {
        HttpContext contextWithBase;
        string openid = "o0Kjx5LdYypgMxW95RvPQf3HnH5E";                 // 微信获取到的微信id

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            mainManager();
        }
        #endregion


        #region 主方法
        public void mainManager() { 
            // 1. 根据当前的openid 获取签到表的信息
            Sign_Action signAction = new Sign_Action();
            List<Class_Model>classList = signAction.getCurrentTodaySignClassInfoManager(openid);
            successManager(classList);
        
            // 2. 根据签到表的信息返回今天课程表

        }
        #endregion


        #region 执行方法
        public void successManager(List<Class_Model> classList)
        {
            OUTClass_List_Model out_base_setting = new OUTClass_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Class_List_Model listModel = new Class_List_Model();
            listModel.list = classList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}