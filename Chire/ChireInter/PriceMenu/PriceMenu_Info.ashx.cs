﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.PriceMenu
{
    /// <summary>
    /// PriceMenu_Info 的摘要说明
    /// </summary>
    public class PriceMenu_Info : IHttpHandler
    {
        HttpContext contextWithBase;

        string id = "";             // 课程名字
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
          

            getMenuInfoManager();
        }
        #endregion


        private void getMenuInfoManager() {
            PriceMenu_Action priceMenuAction = new PriceMenu_Action();
            PriceMenu_Model priceMenuModel = priceMenuAction.getPriceMenuWithId(id);
            successManager(priceMenuModel);

        }

        #region 成功方法
        public void successManager(PriceMenu_Model priceMenuModel)
        {
            Out_PriceMenu_Model out_base_setting = new Out_PriceMenu_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = priceMenuModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}