﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.PriceMenu
{
    /// <summary>
    /// PriceMenu_List 的摘要说明
    /// </summary>
    public class PriceMenu_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            PriceMenu_Action priceMenuAction = new PriceMenu_Action();

            List<PriceMenu_Model> priceMenuList = priceMenuAction.getPriceMenuListManager();
            successManager(priceMenuList);

        }
        #endregion


        #region 成功方法
        public void successManager( List<PriceMenu_Model> priceListModel)
        {
            Out_Parent_List_Model out_base_setting = new Out_Parent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            PriceMenu_List_Model listModel = new PriceMenu_List_Model();
            listModel.list = priceListModel;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
     
    }
}