﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Add 的摘要说明
    /// </summary>
    public class News_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string des = "";
        string type = "";
        string ablum = "";
        string link = "";


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
                  if (contextWithBase.Request.Form.AllKeys.Contains("des"))
            {
                des = contextWithBase.Request.Form["des"];
            }
             if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }
                   if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }
                if (contextWithBase.Request.Form.AllKeys.Contains("link"))
            {
                link = contextWithBase.Request.Form["link"];
            }
        
            ablum= "https://chire.oss-cn-hangzhou.aliyuncs.com/smart/users/157663712436596.png";
des= "试试水";
link= "2";
name= "试试水";
type= "1";


            insertIntoNews();
        }
        #endregion

        #region 插入数据
        public void insertIntoNews()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into news_list(name,des,type,ablum,link,createtime) values(@name,@des,@type,@ablum,@link,@createtime)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            cmd.Parameters.Add("@des", SqlDbType.VarChar, 350);
            cmd.Parameters["@des"].Value = des;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 50);
            cmd.Parameters["@type"].Value = type;
            cmd.Parameters.Add("@ablum", SqlDbType.VarChar, 350);
            cmd.Parameters["@ablum"].Value = ablum;
            cmd.Parameters.Add("@link", SqlDbType.VarChar, 350);
            cmd.Parameters["@link"].Value = link;
            cmd.Parameters.Add("@createtime", SqlDbType.VarChar, 350);
            cmd.Parameters["@createtime"].Value = DateTime.Now.ToString();
           
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
          
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}