﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Subject
{
    /// <summary>
    /// Subject_Info 的摘要说明
    /// </summary>
    public class Subject_Info : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;

            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }

            id = "14";
            insertSubjectInfoManager();
        }
        #endregion


        #region 查看科目
        public void insertSubjectInfoManager()
        {
            Subject_Action subjectAction = new Subject_Action();
            Subject_Model subjectModel = subjectAction.getSubjectInfoWithId(id);
            successManager(subjectModel);
        }
        #endregion

        #region 成功方法
        public void successManager(Subject_Model subjectModel)
        {
            OUTSubject_Model out_base_setting = new OUTSubject_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = subjectModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}