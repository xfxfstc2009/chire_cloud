﻿using Chire.ChireInter.Students;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Event_calendar
{
    /// <summary>
    /// 获取某事件对应关联的学生门
    /// </summary>
    public class Event_List_Student : IHttpHandler
    {
        HttpContext contextWithBase;
        string eventId = "vbndiedzj6cvwmd";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("eventId"))
            {
                eventId = contextWithBase.Request.Form["eventId"];
            }
       
            delEventWithEvent();
        }
        #endregion

        #region 插入数据
        public void delEventWithEvent()
        {
            Event_Action eventAction = new Event_Action();
            List<Event_Student> eventStudentList = eventAction.getEventStudentList(eventId);
        
           successManager(eventStudentList);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Event_Student> list)
        {
            outEvent_Student_List out_base_setting = new outEvent_Student_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Event_Student_List studentList = new Event_Student_List();
            studentList.list = list;
            out_base_setting.data = studentList;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}