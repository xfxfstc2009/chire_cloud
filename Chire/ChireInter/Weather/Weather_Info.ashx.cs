﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Chire.wechat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Weather
{
    /// <summary>
    /// Weather_Info 的摘要说明
    /// </summary>
    public class Weather_Info : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            mainManager();
        }

        #region 进行执行
        public void mainManager() {
            string newUrl = "http://restapi.amap.com/v3/weather/weatherInfo?key=" + Constance.amapKey + "&city=330109";
            string info = HttpGet(newUrl, "");

            JObject jo = (JObject)JsonConvert.DeserializeObject(info);
        
            JArray array = (JArray)jo["lives"];
            string city = array[0]["city"].ToString();
            string weather = array[0]["weather"].ToString();
            string temperature = array[0]["temperature"].ToString();
            string datetime = array[0]["reporttime"].ToString();

            string weatherType = "";
            if (weather.IndexOf("雪") > -1)
            {
                weatherType = "雪";
            }
            else if (weather.IndexOf("雷") > -1) {
                weatherType = "雷";
            }
            else if (weather.IndexOf("雨") > -1)
            {
                weatherType = "雨";
            }
            else if (weather.IndexOf("风") > -1)
            {
                weatherType = "风";
            }
            else {
                weatherType = "阳";
            }

            string[] timeArr = datetime.Split(' ');
            string date = timeArr[0];
            string time = timeArr[1];

            Weather_Model weatherModel = new Weather_Model();
            weatherModel.city = city;
            weatherModel.weather = weather;
            weatherModel.weatherType = weatherType;
            weatherModel.date = date;
            weatherModel.time = time;
            weatherModel.temperature = temperature;

            successManager(weatherModel);
        }
        
        #endregion

        #region 成功方法
        public void successManager(Weather_Model weatherModel)
        {
            OUTWeather_Model out_base_setting = new OUTWeather_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = weatherModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 1.Get请求

        public static string HttpGet(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8);
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }

        #endregion
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
        
    }
}