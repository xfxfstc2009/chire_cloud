﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using BeeTV.Classes.Other.Model;
using Newtonsoft.Json;
using System.IO;

namespace BeeTV.Classes.Other.Controller
{
    /// <summary>
    /// Other_Shenhe 的摘要说明
    /// </summary>
    public class Other_Shenhe : IHttpHandler
    {
        HttpContext contextWithBase;
        string appName = "Chire";
        string appVersion = "1.0";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("appName"))
            {
                appName = contextWithBase.Request.Form["appName"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("appVersion"))
            {
                appVersion = contextWithBase.Request.Form["appVersion"];
            }

            searchCurrentType();
        }
        #endregion

        #region 获取版本
        public void searchCurrentType()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from company_shenhe where appName = '" + appName + "' and appVersion = '" + appVersion + "' order by id desc ";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            ShenheModel shenheModel = new ShenheModel();

            if (dr.Read())
            {
                int isShenhe = Convert.ToInt32(dr["isShenhe"].ToString());              // 博客
                string lungch_img = dr["lungch_img"].ToString();
                string lungch_content = dr["lungch_content"].ToString();
                string login_delegate = dr["login_delegate"].ToString();
                int show_type = Convert.ToInt32(dr["show_type"].ToString());
          

                shenheModel.lungch_img = lungch_img;
                shenheModel.lungch_content = lungch_content;
                shenheModel.login_delegate = login_delegate;
                shenheModel.show_type = show_type;
                shenheModel.isShenhe = isShenhe;
                shenheManager(shenheModel);
            }
            else
            {
                shenheModel.lungch_img = ""; ;
                shenheModel.lungch_content = "";
                shenheModel.isShenhe = 0 ;
                shenheManager(shenheModel);
            }
            sqlcon.Close();
          
           
        }
        #endregion


        #region 执行方法
        public void shenheManager(ShenheModel shenheModel)
        {
            OutShenheModel out_base_setting = new OutShenheModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";

            ShenheModel model = shenheModel;


            out_base_setting.data = model;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion


    }
}