﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using BeeTV.Classes.Other.Model;
using Newtonsoft.Json;
using System.IO;

namespace BeeTV_1.Classes.Other.Controller
{
    /// <summary>
    /// Other_Shenhe_List 的摘要说明
    /// </summary>
    public class Other_Shenhe_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string appName = "beeTV";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("appName"))
            {
                appName = contextWithBase.Request.Form["appName"];
            }

            getShenheInfo();
        }
        #endregion


        #region 获取gift列表
        public void getShenheInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string strselect = "select * from company_shenhe where appName = '" + appName + "'";

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<ShenheModel> shenheList = new List<ShenheModel>();
            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string appName1 = dt.Rows[i]["appName"].ToString();
                string appVersion = dt.Rows[i]["appVersion"].ToString();
                string lungch_img = dt.Rows[i]["lungch_img"].ToString();
                string lungch_content = dt.Rows[i]["lungch_content"].ToString();
                int show_type = Convert.ToInt32(dt.Rows[i]["show_type"].ToString());
                int isShenhe = Convert.ToInt32(dt.Rows[i]["isShenhe"].ToString());
                string login_delegate = dt.Rows[i]["login_delegate"].ToString();
                ShenheModel giftModel = new ShenheModel { id = id, appName = appName1, appVersion = appVersion, lungch_img = lungch_img, lungch_content = lungch_content, show_type = show_type, isShenhe = isShenhe, login_delegate= login_delegate };
                shenheList.Add(giftModel);
            }
            sqlcon.Close();
            successManager(shenheList);
        }
        #endregion

        #region 输出方法
        public void successManager(List<ShenheModel> shenheList)
        {
            OUTShenheListModel out_base_setting = new OUTShenheListModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            ShenheListModel listModel = new ShenheListModel();
            listModel.list = shenheList;
           
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}