﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using BeeTV_1.Classes.Other.Model;
using System.Data;
using Newtonsoft.Json;
using System.IO;

namespace BeeTV_1.Classes.Other.Controller
{
    /// <summary>
    /// Other_Share_Insert 的摘要说明
    /// </summary>
    public class Other_Share_Insert : IHttpHandler
    {
        HttpContext contextWithBase;
        string title = "";
        string desc = "";
        string imgs = "";
        string url = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("title"))
            {
                title = contextWithBase.Request.Form["title"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("desc"))
            {
                desc = contextWithBase.Request.Form["desc"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("imgs"))
            {
                imgs = contextWithBase.Request.Form["imgs"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }


            insertShare();
        }
        #endregion

        #region 执行数据操作
        public void insertShare() {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into other_share(title,des,imgs,url,time,share_type) values(@title,@des,@imgs,@url,@time,@share_type)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            Share_SingleModel shareModel = new Share_SingleModel();
            // 昵称
            cmd.Parameters.Add("@title", SqlDbType.NVarChar);
            cmd.Parameters["@title"].Value = title;
            shareModel.title = title;

            // 性别
            cmd.Parameters.Add("@des", SqlDbType.NVarChar);
            cmd.Parameters["@des"].Value = desc;
            shareModel.desc = desc;
            // 年龄
            cmd.Parameters.Add("@imgs", SqlDbType.NVarChar);
            cmd.Parameters["@imgs"].Value = imgs;
            shareModel.imgs = imgs;

            cmd.Parameters.Add("@url", SqlDbType.NVarChar);
            cmd.Parameters["@url"].Value = url;
            shareModel.url = url;

            cmd.Parameters.Add("@share_type", SqlDbType.NVarChar);
            cmd.Parameters["@share_type"].Value = 1;
            
            // 时间
            cmd.Parameters.Add("@time", SqlDbType.VarChar, 50);
            cmd.Parameters["@time"].Value = DateTime.Now.ToString();


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            // [去执行成功方法]
            shareManager(shareModel);
        }
        #endregion


        #region 执行方法
        public void shareManager(Share_SingleModel shenheModel)
        {
            OutShare_SingleModel out_base_setting = new OutShare_SingleModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";

            Share_SingleModel model = shenheModel;


            out_base_setting.data = model;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}