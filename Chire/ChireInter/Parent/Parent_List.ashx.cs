﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Parent
{
    /// <summary>
    /// Parent_List 的摘要说明
    /// </summary>
    public class Parent_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            getAllParent();
        }

        #region 获取所有的家长
        public void getAllParent() {
            Parent_Action parentAction = new Parent_Action();
            List<Parent_Model> parentList = parentAction.getAllParent();
            successManager(parentList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Parent_Model> parentList)
        {
            Out_Parent_List_Model out_base_setting = new Out_Parent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Parent_List_Model listModel = new Parent_List_Model();
            listModel.list = parentList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}