﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher.Jurisdiction
{
    public class Jurisdiction_Action
    {

        #region 初始化添加老师权限
        public void initTeacherJurisdiction(string linkId)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into teacher_jurisdiction(sign,show,linkId) values";
            StrInsert += "('视图管理',1,'" + linkId + "'),";
            StrInsert += "('学生管理',1,'" + linkId + "'),";
            StrInsert += "('班级管理',1,'" + linkId + "'),";
            StrInsert += "('箴言管理',1,'" + linkId + "'),";
            StrInsert += "('学院管理',1,'" + linkId + "'),";
            StrInsert += "('作业管理',1,'" + linkId + "'),";
            StrInsert += "('留言管理',1,'" + linkId + "'),";
            StrInsert += "('老师管理',1,'" + linkId + "'),";
            StrInsert += "('签到管理',1,'" + linkId + "'),";
            StrInsert += "('活动管理',1,'" + linkId + "'),";
            StrInsert += "('学习管理',1,'" + linkId + "'),";
            StrInsert += "('测验管理',1,'" + linkId + "'),";
            StrInsert += "('协议管理',1,'" + linkId + "'),";
            StrInsert += "('科目管理',1,'" + linkId + "'),";
            StrInsert += "('认证管理',1,'" + linkId + "')";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
         
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 添加老师权限
        public void addTeacherJurisdiction(string sign, int show, string linkId)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into teacher_jurisdiction(sign,show,linkId) values(@sign,@show,@linkId)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@sign", SqlDbType.NVarChar);
            cmd.Parameters["@sign"].Value = sign;

            cmd.Parameters.Add("@show", SqlDbType.NVarChar);
            cmd.Parameters["@show"].Value = show;

            cmd.Parameters.Add("@linkId", SqlDbType.NVarChar);
            cmd.Parameters["@linkId"].Value = linkId;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 获取老师权限
        public List<Jurisdiction_Model> getJurisdictionList(string linkId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher_jurisdiction where linkId = '"+linkId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;


            List<Jurisdiction_Model> jurisdictionList = new List<Jurisdiction_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string sign = dt.Rows[i]["sign"].ToString();
                int show = Convert.ToInt32(dt.Rows[i]["show"].ToString());
                Jurisdiction_Model jurisdictionModel = new Jurisdiction_Model()
                {
                   sign = sign,
                   show = show,
                };
                jurisdictionList.Add(jurisdictionModel);
            }
            sqlcon.Close();
            return jurisdictionList;
        }
        #endregion

        #region 修改老师权限
        public void updateTeacherJurisdiction(string sign,int show, string linkId) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息

            string strSqls = "update teacher_jurisdiction set show = " + show + "  where linkId ='" + linkId + "' and sign = '"+sign+"'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion
    }
}