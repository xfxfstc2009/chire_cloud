﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Img_Add 的摘要说明
    /// </summary>
    public class Teacher_Img_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        string imgs = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("imgs"))
            {
                imgs = contextWithBase.Request.Form["imgs"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            addTeacherImg();
        }
        #endregion

        #region 添加老师资历
        public void addTeacherImg()
        {
            Teacher_Action teacherAction = new Teacher_Action();
            string[] imgArr = imgs.Split(',');
            List<string> imgList = imgArr.ToList<string>();
            for (int i = 0; i < imgList.Count; i++) {
                string imgStr = imgList[i];
                teacherAction.teacherAddImg(id, imgStr);
            }

            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}