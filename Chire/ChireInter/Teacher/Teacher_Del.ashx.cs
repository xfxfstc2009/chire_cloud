﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Del 的摘要说明
    /// </summary>
    public class Teacher_Del : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }

            searchAll();
        }
        #endregion

        #region 查询学生表
        public void searchAll()
        {
            Teacher_Action teacherAction = new Teacher_Action();
            teacherAction.deleteTeacher(id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}