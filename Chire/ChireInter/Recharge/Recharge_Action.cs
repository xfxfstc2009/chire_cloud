﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Login;

namespace Chire.ChireInter.Recharge
{
    public class Recharge_Action
    {
        public void addRechargeManager(string openid, string studentId,string priceMenuId,string money) { 
            // 1. 判断是否是管理员
            Login_Action loginAction = new Login_Action();
            bool hasAdmin = loginAction.hasAdminManager(openid);

            // 2. 授权是管理员
            if (hasAdmin == true) {
                // 连接数据库
                SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
                sqlcon.Open();
                string StrInsert = "";
              
                StrInsert = "insert into recharge_history(name,user_id,priceMenu_id,money,create_time) values(@name,@user_id,@priceMenu_id,@money,@create_time)";
                SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
                // 添加参数并且设置参数值
                // 1. 作者id
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = "学费充值";

                cmd.Parameters.Add("@user_id", SqlDbType.VarChar, 50);
                cmd.Parameters["@user_id"].Value = studentId;

                cmd.Parameters.Add("@priceMenu_id", SqlDbType.VarChar, 50);
                cmd.Parameters["@priceMenu_id"].Value = priceMenuId;

                cmd.Parameters.Add("@money", SqlDbType.VarChar, 50);
                cmd.Parameters["@money"].Value = money;

                cmd.Parameters.Add("@create_time", SqlDbType.VarChar, 50);
                cmd.Parameters["@create_time"].Value = DateTime.Now.ToString();

                // 执行插入数据的操作
                cmd.ExecuteNonQuery();
                sqlcon.Close();
            }
        }
    }
}