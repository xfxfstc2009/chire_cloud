﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Parent;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;
using Chire.ChireInter.Wechat.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Wechat
{
    public class Wechat_Action
    {
        #region 根据code 获取openid
        public string getOpenIdWithCode(string code)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string dateNow = DateTime.Now.ToShortDateString().ToString();

            string strselect = "select top 1 * from wechat_power where code = '" + code + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            string openid = "";
            if (dr.Read())
            {
                string info = dr["info"].ToString();
                // 2. 获取openid
                JObject jo = (JObject)JsonConvert.DeserializeObject(info);
                openid = jo["openid"].ToString();
            }
            sqlcon.Close();
            return openid;
        }
        #endregion

        #region 微信提交地址
        // 0. 微信地址主方法
        public void wechatLocationMainManager(string openid, string lat, string lon)
        {
            bool hasLocation = wechatHasLocation(openid);
            if (hasLocation == true)
            {
                wechatUpdateLocation(openid, lat, lon);
            }
            else
            {
                wechatInsertLocation(openid, lat, lon);
            }
        }
        // 1. 是否有微信地址
        private bool wechatHasLocation(string openid)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from wechat_location where openid = '" + openid + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            bool hasWechat = false;
            if (dr.Read())
            {
                hasWechat = true;
            }
            sqlcon.Close();
            return hasWechat;
        }

        // 2. 插入微信地址
        private void wechatInsertLocation(string openid, string lat, string lon)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into wechat_location(openid,lat,lon) values(@openid,@lat,@lon)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@openid", SqlDbType.VarChar, 50);
            cmd.Parameters["@openid"].Value = openid;

            cmd.Parameters.Add("@lat", SqlDbType.VarChar, 50);
            cmd.Parameters["@lat"].Value = lat;
            // 2.日记详情
            cmd.Parameters.Add("@lon", SqlDbType.VarChar, 50);
            cmd.Parameters["@lon"].Value = lon;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }

        // 3. 修改微信地址
        private void wechatUpdateLocation(string openid, string lat, string lon)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "";
            strSqls = "update wechat_location set lat = '" + lat + "', lon = '" + lon + "' where openid = '" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 分页获取当前的消息
        public void getWechatNoticePage() { 
            
        }
        #endregion

        #region 分页获取当前的学生地址
        public List<Wechat_Location_Model> locationGetStudentList()
        { 
          SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from wechat_location";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            Student_Action studentAction = new Student_Action();

            List<Wechat_Location_Model> location_List = new List<Wechat_Location_Model>();
            for (int i = 0; i < rows; i++)
            {
        
                string id = dt.Rows[i]["id"].ToString();
                string openid = dt.Rows[i]["openid"].ToString();
                string lat = dt.Rows[i]["lat"].ToString();
                       string lng = dt.Rows[i]["lon"].ToString();
                       Student_Model studentModel = studentAction.getStudentInfoWithStudentOpenId(openid);

                Wechat_Location_Model locationModel = new Wechat_Location_Model() { id=id,openid = openid,lat=lat,lng = lng,student = studentModel };
                location_List.Add(locationModel);
            }
            sqlcon.Close();
            return location_List;
        }
        #endregion

  
    }


}