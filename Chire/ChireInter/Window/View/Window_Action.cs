﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Window.View
{
    public class Window_Action
    {
        #region 修改某地方的图片
        public void windowChangeImg(string title,string imgUrl) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update window_img_setting set url = '"+imgUrl+"'  where title ='" + title + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        } 
    	#endregion

        #region 获取列表
        public List<Window_List_Single> windowGetAllImgList()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from window_img_setting";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Window_List_Single> listModel = new List<Window_List_Single>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string title = dt.Rows[i]["title"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string url = dt.Rows[i]["url"].ToString();

                string type = dt.Rows[i]["type"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();


                Window_List_Single singleModel = new Window_List_Single()
                {
                    id = id,
                    name = name,
                    title = title,
                    url = url,
                    type = type,
                    datetime = datetime
                };
                listModel.Add(singleModel);
            }
            sqlcon.Close();
            return listModel;
        }
        #endregion

    }
}