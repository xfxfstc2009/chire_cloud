﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Order
{
    /// <summary>
    /// Order_Single_List 的摘要说明
    /// </summary>
    public class Order_Single_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "";
        string openid = "";                 // 微信获取到的微信id
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
  
            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager() {
            // 获取1.openid
            Wechat_Action wechatAction = new Wechat_Action();
            openid = wechatAction.getOpenIdWithCode(code);
            // 2.根据openid获取studentid
            Student_Action studentAction = new Student_Action();
            Student_Model studentModel = studentAction.getStudentIdWithOpenId(openid);
            // 3. 根据学生id 获取列表
            Order_Action orderAction = new Order_Action();
            List<Order_Model> orderListWillStart = orderAction.getAllOrderManagerWithUserId(studentModel.id, "即将开课");
            List<Order_Model> orderListEnd = orderAction.getAllOrderManagerWithUserId(studentModel.id, "已签退");
            successManager(orderListWillStart, orderListEnd);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Order_Model> orderListWillStart, List<Order_Model> orderListEnd)
        {
            Out_Order_Model out_base_setting = new Out_Order_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Order_List_Model orderListModel = new Order_List_Model();
            orderListModel.willStartList = orderListWillStart;
            orderListModel.endList = orderListEnd;

            out_base_setting.data = orderListModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}