﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Order
{
    public class Order_Action
    {
        #region 即将开课，生成开课记录
        public void order_willStartGetList(string classid) {
            Class_Action classAction = new Class_Action();
            Class_Model classModel = classAction.getClassInfoWithId(classid);
            // 1. 获取班内所有的学生
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.getAllStudent("1", classid);
            for (int i = 0; i < studentList.Count; i++) {
                Student_Model studentModel = studentList[i];
                Order_Model model = new Order_Model();
    
                model.name = "【待上】"+classModel.name;
                model.student_id = studentModel.id;
                model.class_id = classid;
                model.order_type = "支出";
                model.order_time = 0;
                model.detail = "";
                model.date = DateTime.Now.ToShortDateString();
                model.time = DateTime.Now.ToString();
                model.status = "即将开课";
                addClassInfo(model);
            }
        }
        #endregion

        #region 已经签退，计算课时
        public void signOutCalculationClassTime(string classid,string student_id)
        {
            // 1. 获取课程信息
            Class_Action classAction = new Class_Action();
            Class_Model classModel = classAction.getClassInfoWithId(classid);

            // 1. 获取班内所有的学生
            Student_Action studentAction = new Student_Action();

            Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
            Order_Model model = new Order_Model();

            model.name = "【已结算】" + classModel.name;
            model.student_id = studentModel.id;
            model.class_id = classid;
            model.order_type = "支出";

            DateTime dt = Convert.ToDateTime(classModel.begintime);          // 开课时间
            int beginHour = dt.Hour;
            int beginMin = dt.Minute;
            double beginMinF = beginMin * 1.0 / 60;
            double beginMinTime = beginHour + beginMinF;

            DateTime dt1 = Convert.ToDateTime(classModel.endtime);          // 结束时间
            int endHour = dt1.Hour;
            int endMin = dt1.Minute;
            double endMinF = endMin * 1.0 / 60;
            double endMinTime = endHour + endMinF;

            model.order_time = (endMinTime - beginMinTime);
            model.detail = "开课时间为：" + classModel.begintime + "签退时间为：" + DateTime.Now.ToString();
            model.date = DateTime.Now.ToShortDateString();
            model.time = DateTime.Now.ToString();
            model.status = "已签退";
            if (searchHasOrsder(model) == true)
            {
                updateCurrentOrderInfo(model);
            }
            else
            {
                addClassInfo(model);
            }
        
        }
        #endregion

        #region 插入课程信息
        public void addClassInfo(Order_Model model) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into time_order(name,student_id,class_id,order_type,order_time,detail,date,time,status) values (@name,@student_id,@class_id,@order_type,@order_time,@detail,@date,@time,@status)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = model.name;
            // 1. 日记标题
            cmd.Parameters.Add("@student_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@student_id"].Value = model.student_id;
            // 2.日记详情
            cmd.Parameters.Add("@class_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@class_id"].Value = model.class_id;

            cmd.Parameters.Add("@order_type", SqlDbType.VarChar, 50);
            cmd.Parameters["@order_type"].Value = model.order_type;

            cmd.Parameters.Add("@order_time", SqlDbType.Float, 50);
            cmd.Parameters["@order_time"].Value = model.order_time;

            cmd.Parameters.Add("@detail", SqlDbType.VarChar, 1000);
            cmd.Parameters["@detail"].Value = model.detail;

            cmd.Parameters.Add("@date", SqlDbType.VarChar, 1000);
            cmd.Parameters["@date"].Value = model.date;

            cmd.Parameters.Add("@time", SqlDbType.VarChar, 1000);
            cmd.Parameters["@time"].Value = model.time;

            cmd.Parameters.Add("@status", SqlDbType.VarChar, 1000);
            cmd.Parameters["@status"].Value = model.status;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 查询是否有当前信息
        public bool searchHasOrsder(Order_Model model) {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from time_order where date = '" + model.date + "' and student_id = '" + model.student_id + "' and class_id = '" + model.class_id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            bool hasRead = false;
            if (dr.Read())
            {
                hasRead = true;
            }
            sqlcon.Close();

            return hasRead;
        }
        #endregion

        #region 修改当前信息-已签退
        public void updateCurrentOrderInfo(Order_Model model)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "";
            strSqls = "update time_order set name = '"+model.name+"', status = '"+model.status+"', order_time = '" + model.order_time + "',detail='" + model.detail + "'  where date = '" + model.date + "' and student_id = '" + model.student_id + "' and class_id = '" + model.class_id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }

        #endregion

        #region 获取当前所有的订单
        public List<Order_Model> getAllOrderManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from time_order order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Order_Model> orderList = new List<Order_Model>();
            Student_Action studentAction = new Student_Action();
            Class_Action classAction = new Class_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
              
                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);

                string order_type = dt.Rows[i]["order_type"].ToString();
                double order_time = Convert.ToDouble(dt.Rows[i]["order_time"].ToString());
                string detail = dt.Rows[i]["detail"].ToString();
                string date = dt.Rows[i]["date"].ToString();
                string time = dt.Rows[i]["time"].ToString();
                string status = dt.Rows[i]["status"].ToString();
                Order_Model orderModel = new Order_Model() {id = id,name = name,student = studentModel,classModel = classModel,
                order_type = order_type,order_time = order_time,detail = detail,date = date,time = time,status = status };
                orderList.Add(orderModel);
            }
            sqlcon.Close();
            return orderList;
        }
        #endregion

        #region 获取当前用户所有的订单
        public List<Order_Model> getAllOrderManagerWithUserId(string studentId,string requestStatus)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from time_order where student_id = '" + studentId + "' and status = '" + requestStatus + "' order by time desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Order_Model> orderList = new List<Order_Model>();
            Student_Action studentAction = new Student_Action();
            Class_Action classAction = new Class_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);

                string order_type = dt.Rows[i]["order_type"].ToString();
                double order_time = Convert.ToDouble(dt.Rows[i]["order_time"].ToString());
                string detail = dt.Rows[i]["detail"].ToString();
                string date = dt.Rows[i]["date"].ToString();
                string time = dt.Rows[i]["time"].ToString();
                string status = dt.Rows[i]["status"].ToString();
                Order_Model orderModel = new Order_Model()
                {
                    id = id,
                    name = name,
                    student = studentModel,
                    classModel = classModel,
                    order_type = order_type,
                    order_time = order_time,
                    detail = detail,
                    date = date,
                    time = time,
                    status = status
                };
                orderList.Add(orderModel);
            }
            sqlcon.Close();
            return orderList;
        }
        #endregion
    }
}